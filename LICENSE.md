<!--
SPDX-FileCopyrightText: 2024 German Aerospace Center (DLR)
SPDX-License-Identifier: MIT
-->


## License

Copyright © 2024 German Aerospace Center (DLR)

This work is licensed under multiple licenses:
- The source code and the accompanying material are licensed under [MIT](LICENSES/MIT.txt).
- The documentation and the resulting plots are licensed under [CC-BY-4.0](LICENSES/CC-BY-4.0.txt).

Please see the individual files for more accurate information.

> **Hint:** We provided the copyright and license information in accordance to the [REUSE Specification 3.0](https://reuse.software/spec/).
