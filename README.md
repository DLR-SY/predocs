# PreDoCS: Preliminary Design of Composite Structures

PreDoCS (Preliminary Design of Composite Structures) is a Python tool for fast evaluation of
wing structure concepts i.e. for a multi-disciplinary design optimization.

PreDoCS can be obtained via [pypi](https://pypi.org/project/predocs/).
For further information refer to the [Documentation](https://dlr-sy.gitlab.io/predocs/).


## Run example notebooks

### `Cross Section Analysis/index.ipynb`

This notebook shows the general use of the PreDoCS cross section calculation and has only PreDoCS as dependency.


### `Validation/Cross Section Analysis/Paper-public.ipynb`

This notebook creates the data and plots for the PreDoCS paper.
To run it, the following dependencies additionally to PreDoCS must be installed:


Install [octave](https://octave.org/) and add the `bin` or `mingw64/bin` directory to the `PATH`.


Install [BECAS](https://becas.dtu.dk/), set `BECAS_PATH` environment variable.


Install [shellexpander](https://becas.dtu.dk/software/pre-and-post-processors/shellexpander) to the python environment.


Install PARAVIEW via conda

    conda install -c conda-forge paraview

## License

Please see the file [LICENSE.md](LICENSE.md) for further information about how the content is licensed.