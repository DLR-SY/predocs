#   Copyright (c): 2024 Deutsches Zentrum fuer Luft- und Raumfahrt (DLR, German Aerospace Center) <www.dlr.de>. All rights reserved.

import argparse

import os
import sys
sys.path.append(os.path.dirname(__file__))

# Import the simple module from the paraview
from paraview.simple import *

from paraview_utils import apply_tensor_calculation_filter

# Disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# Get parser for command line arguments
parser = argparse.ArgumentParser(description='Plots the load states of the cross section elements for different load cases.')
parser.add_argument('-r', '--becas_results_case_format_string', action='store', required=True,
                   help='The format string to the becas results case. {0} for the cross section, {1} for the load case.')
parser.add_argument('-o', '--output_format_string', action='store', required=True,
                   help='The format string for the output files. {0} for the cross section, {1} for the load case, {2} plot name.')
parser.add_argument('-f', '--full_plots', action='store_true',
                   help='For plotting all plots, not only normal flow and membrane shear flow.')
parser.add_argument('-g', '--glyph_scale_factor', action='store', type=float, default=0.01,
                   help='The scale factor for the glyphs.')
parser.add_argument('-b', '--max_bar_length', action='store', type=float, default=0.3,
                   help='The max length of a value bar.')
parser.add_argument('-l', '--load_case', dest='load_cases', action='store', nargs='+', required=True,
                   help='The load_cases to plot.')
parser.add_argument('-c', '--cross_section', dest='cross_sections', action='store', nargs='+', required=True,
                   help='The cross sections to plot.')
parser.add_argument('-s', '--stride', dest='strides', action='store', type=int, nargs='+', required=True,
                   help='The stride for each cross section.')
args = parser.parse_args()
print(args)

# Get aruments
becas_results_case_format_string = args.becas_results_case_format_string
output_format_string = args.output_format_string
glyph_scale_factor = args.glyph_scale_factor
max_bar_length = args.max_bar_length
profiles = args.cross_sections
strides = args.strides
assert(len(profiles) == len(strides))
load_cases = args.load_cases#['transverse_x', 'transverse_y', 'extension', 'bending_x', 'bending_y', 'torsion']
plots = {'sigma_z': ('stress_zz', None, 'sigma_z [N/m^2 = Pa]'),
         'sigma_zs': ('stress_zs', 'element_s', 'sigma_zs [N/m^2 = Pa]')}
if args.full_plots:
    plots.update({'epsilon_s': ('strain_ss', None, 'epsilon_s [-]'),
                  'epsilon_z': ('strain_zz', None, 'epsilon_z [-]'),
                  'epsilon_n': ('strain_nn', None, 'epsilon_n [-]'),
                  'gamma_zs': ('strain_zs', 'element_s', 'gamma_zs [-]'),
                  'gamma_sn': ('strain_sn', None, 'gamma_sn [-]'),
                  'gamma_zn': ('strain_zn', 'element_n', 'gamma_zn [-]'),
                  'sigma_s': ('stress_ss', None, 'sigma_s [N/m^2 = Pa]'),
                  'sigma_n': ('stress_nn', None, 'sigma_n [N/m^2 = Pa]'),
                  'sigma_sn': ('stress_sn', None, 'sigma_sn [N/m^2 = Pa]'),
                  'sigma_zn': ('stress_zn', 'element_n', 'sigma_zn [N/m^2 = Pa]')})

for profile in profiles:
    for load_case in load_cases:
        # For each result set
        
        # Read data
        input_file = becas_results_case_format_string.format(profile, load_case)
        print(input_file)
        resultscase = EnSightReader(CaseFileName=input_file,
                                          CellArrays=['material_id', 'material_ori_1', 'material_ori_2', 'material_ori_3', 'elementnumbers', 'strain11', 'strain22', 'strain12', 'strain13', 'strain23', 'strain33', 'stress11', 'stress22', 'stress12', 'stress13', 'stress23', 'stress33', 'failure11', 'failure22', 'failure12', 'failure13', 'failure23', 'failure33'],
                                          PointArrays=['nodenumbers', 'elastic_axis_1', 'elastic_axis_2', 'warping'])
        #print(resultscase)
        if not resultscase:
            raise RuntimeError('Error while loading files')
        
        # Tensor calculation
        input_filter = apply_tensor_calculation_filter(resultscase, max_bar_length)

        #print(input_filter.CellData.keys())
        # Get cell data info
        cell_data_information = input_filter.GetCellDataInformation()
        #print(cell_data_information.keys())
        
        # For each plot
        for (k, v) in plots.items():
            plot_name = k
            plot_value = v[0]
            arrow_direction = v[1]
            value_label = v[2]
            
            # Get active view
            render_view = CreateRenderView()
            render_view.InteractionMode = '2D'
            render_view.ViewSize = [1000, 750]
            
            # Background
            render_view.Background = [1.0, 1.0, 1.0]
            
            # Axis grid
            render_view.OrientationAxesVisibility = 0
            #render_view.OrientationAxesLabelColor = [0.0, 0.0, 0.0]
            #render_view.CenterAxesVisibility = 1
            render_view.AxesGrid.Visibility = 1
            render_view.AxesGrid.XTitle = 'x_crosssection [m]'
            render_view.AxesGrid.YTitle = 'y_crosssection [m]'
            render_view.AxesGrid.ZTitle = ''
            render_view.AxesGrid.XTitleColor = [0.0, 0.0, 0.0]
            render_view.AxesGrid.YTitleColor = [0.0, 0.0, 0.0]
            render_view.AxesGrid.ZTitleColor = [0.0, 0.0, 0.0]
            render_view.AxesGrid.GridColor = [0.0, 0.0, 0.0]
            render_view.AxesGrid.XLabelColor = [0.0, 0.0, 0.0]
            render_view.AxesGrid.YLabelColor = [0.0, 0.0, 0.0]
            render_view.AxesGrid.ZLabelColor = [0.0, 0.0, 0.0]
            

            print(plot_value)
            # Get data range
            value_range = cell_data_information[plot_value].GetComponentRange(0)
            print(value_range)
            
            if arrow_direction is None:
                # If no arrows to plot
                
                # Get color transfer function/color map and set range
                valueLUT = GetColorTransferFunction(plot_value)
                #print(cell_data_information[plot_value])
                if value_range[0] == 0.0 and value_range[1] == 0.0:
                    valueLUT.RGBPoints = [0.0, 0.8, 0.8, 0.8]
                elif value_range[0] == 0.0 or (value_range[0] > 0 and value_range[1] > 0):
                    valueLUT.RGBPoints = [0.0, 0.8, 0.8, 0.8, value_range[1], 0.0, 1, 0.0]
                elif value_range[1] == 0.0 or (value_range[0] < 0 and value_range[1] < 0):
                    valueLUT.RGBPoints = [value_range[0], 1.0, 0.0, 0.0, 0.0, 0.8, 0.8, 0.8]
                else:
                    valueLUT.RGBPoints = [value_range[0], 1.0, 0.0, 0.0, 0.0, 0.8, 0.8, 0.8, value_range[1], 0.0, 1, 0.0]
                
                # Show data on surface
                value_display = Show(input_filter, render_view)
                value_display.Representation = 'Surface'
                value_display.ColorArrayName = ['CELLS', plot_value]
                value_display.LookupTable = valueLUT
                
                bar_input = input_filter
                bar_value_name = plot_value                
            else:
                # If arrows to plot

                # Calculator for absolute values
                abs_value = Calculator(Input=input_filter)
                abs_value.AttributeType = 'Cell Data'
                abs_value.ResultArrayName = plot_value+'_abs'
                abs_value.Function = 'abs({})'.format(plot_value)
                
                # Show the absolute values on the surface
                valueLUT = GetColorTransferFunction(plot_value+'_abs')
                abs_value_display = Show(abs_value, render_view)
                abs_value_display.Representation = 'Surface'
                abs_value_display.ColorArrayName = ['CELLS', plot_value+'_abs']
                abs_value_display.LookupTable = valueLUT
                
                # show color bar/color legend
                #abs_value_display.SetScalarBarVisibility(render_view, True)

                bar_input = abs_value
                bar_value_name = plot_value+'_abs'
                
                if value_range[1] == 0.0:
                    # Color if all values are zero, no arrows
                    valueLUT.RGBPoints = [0.0, 0.8, 0.8, 0.8]
                else:
                    # Set color for not ponly zero values
                    valueLUT.RGBPoints = [0.0, 0.8, 0.8, 0.8, value_range[1], 0.0, 1, 0.0]
                    
                    # Create calculator for arrow vector
                    arrow_vector = Calculator(Input=abs_value)
                    arrow_vector.AttributeType = 'Cell Data'
                    arrow_vector.ResultArrayName = plot_value+'_vec'
                    arrow_vector.Function = '{}*{}'.format(arrow_direction, plot_value)
                    
                    # Create arrow glyph
                    arrow_glyph = Glyph(Input=arrow_vector, GlyphType='Cone')
                    #arrow_glyph.Scalars = None
                    arrow_glyph.OrientationArray = ['CELLS', plot_value+'_vec']
                    arrow_glyph.ScaleFactor = glyph_scale_factor
                    arrow_glyph.GlyphMode = 'Every Nth Point'
                    arrow_glyph.Stride = strides[profiles.index(profile)]
                    arrow_glyph.GlyphTransform = 'Transform2'

                    # Set arrow color
                    arrowLUT = GetColorTransferFunction('GlyphScale')
                    arrowLUT.RGBPoints = [0.0, 0.0, 0.0, 0.0]
    
                    # Show arrows
                    arrow_glyph_display = Show(arrow_glyph, render_view)
                    arrow_glyph_display.Representation = 'Surface'
                    arrow_glyph_display.ColorArrayName = ['CELLS', 'GlyphScale']
                    arrow_glyph_display.LookupTable = arrowLUT
                    arrow_glyph_display.Opacity = 0.5
                    arrow_glyph_display.DiffuseColor = [0.0, 0.0, 0.0]
            
            # Show normal value bars if data not only zeros
            max_data_abs_value = (max((abs(value_range[0]), abs(value_range[1]))))
            if max_data_abs_value > 0:
                # Get value bar scale
                value_bar_scale = Calculator(Input=bar_input)  
                value_bar_scale.AttributeType = 'Cell Data'
                value_bar_scale.ResultArrayName = plot_value+'_bars_scale'
                value_bar_scale.Function = 'abs({})'.format(bar_value_name)
                  
                # Create value bar glyph
                value_bar_glyph = Glyph(Input=value_bar_scale, GlyphType='Box')
                value_bar_glyph.GlyphType.Center = [0.5, 0.0, 0.0]
                value_bar_glyph.GlyphType.XLength = 1
                value_bar_glyph.GlyphType.YLength = 0.1
                value_bar_glyph.GlyphType.ZLength = 0.001
                value_bar_glyph.ScaleArray = ['CELLS', plot_value+'_bars_scale']
                value_bar_glyph.OrientationArray = ['CELLS', 'bar_dir']
                value_bar_glyph.ScaleFactor = abs(max_bar_length) / max_data_abs_value
                value_bar_glyph.GlyphMode = 'Every Nth Point'
                value_bar_glyph.Stride = strides[profiles.index(profile)]
                value_bar_glyph.GlyphTransform = 'Transform2'

                # Show value bars
                value_bar_glyph_Display = Show(value_bar_glyph, render_view)
                value_bar_glyph_Display.Representation = 'Surface'
                value_bar_glyph_Display.ColorArrayName = ['POINTS', bar_value_name]
                value_bar_glyph_Display.LookupTable = valueLUT

            # Get color legend/bar for valueLUT
            valueLUTColorBar = GetScalarBar(valueLUT, render_view)
            valueLUTColorBar.Title = value_label
            valueLUTColorBar.ComponentTitle = ''
            valueLUTColorBar.Orientation = 'Horizontal'
            valueLUTColorBar.WindowLocation = 'Lower Right Corner'
            valueLUTColorBar.TitleColor = [0.0, 0.0, 0.0]
            valueLUTColorBar.TitleBold = 1
            valueLUTColorBar.LabelColor = [0.0, 0.0, 0.0]
            valueLUTColorBar.LabelBold = 1
            valueLUTColorBar.ScalarBarThickness = 10
            valueLUTColorBar.ScalarBarLength = 0.5
            
            # Update the view to ensure updated data information
            render_view.Update()
            
            # Reset view to fit data
            render_view.ResetCamera()
            
            # Export view
            file_name = output_format_string.format(profile, load_case, plot_name)
            #ExportView(file_name, view=render_view)
            SaveScreenshot(file_name, render_view, TransparentBackground=0)
            Delete(render_view)
            