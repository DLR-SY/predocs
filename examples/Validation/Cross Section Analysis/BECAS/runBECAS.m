% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Template file for running BECAS
%
% (c) DTU Wind Energy
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
clear
clc

%% Input

profiles = [1];%900:936;%cat(2, 900:936, 1000:1036, 1100:1136, 1200:1236);%600:636;%cat(2, 700:706, [710,711]);% cat(2, 100:106, 200:206, [210,211,220,221,222])%, [500,501]);%300:335;%[501];%[211, 212, 220, 221, 222, 200, 201, 202, 203, 204, 205, 206, 100, 101, 102, 103, 104, 105, 106];%300:335;%[100,200];%400:439;
calc_load_cases = true;
input_path = '../../tmp/comparison/becas/input/';%'../../output/becas_input/';
output_path = '../../tmp/comparison/becas/output/';%'../../output/becas_output/';

%% RUN

BECAS_calc_profiles(profiles, calc_load_cases, input_path, output_path)

