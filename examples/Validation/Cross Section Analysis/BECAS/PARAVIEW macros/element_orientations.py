#   Copyright (c): 2024 Deutsches Zentrum fuer Luft- und Raumfahrt (DLR, German Aerospace Center) <www.dlr.de>. All rights reserved.

# state file generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1944, 1054]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [0.0, 0.0, 0.05000000074505806]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [1.5166408905488233, 1.0043799344117226, 1.3071299539949006]
renderView1.CameraFocalPoint = [1.4191011950159541e-16, -3.989799215399133e-17, 0.0500000007450581]
renderView1.CameraViewUp = [-0.03614674670967053, 0.8015719078111934, -0.596804732982431]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 0.5722976832572465
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(1944, 1054)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'EnSight Reader'
becas_resultscase = EnSightReader(registrationName='becas_results.case', CaseFileName='H:\\Daniel\\git\\PreDoCS\\tmp\\comparison\\becas\\output\\900\\load_cases\\transverse_y\\becas_results.case')
becas_resultscase.CellArrays = ['material_id', 'material_ori_1', 'material_ori_2', 'material_ori_3', 'elementnumbers', 'strain11', 'strain22', 'strain12', 'strain13', 'strain23', 'strain33', 'stress11', 'stress22', 'stress12', 'stress13', 'stress23', 'stress33', 'failure11', 'failure22', 'failure12', 'failure13', 'failure23', 'failure33']
becas_resultscase.PointArrays = ['nodenumbers', 'elastic_axis_1', 'elastic_axis_2', 'warping']

# create a new 'Calculator'
s = Calculator(registrationName='s', Input=becas_resultscase)
s.AttributeType = 'Cell Data'
s.ResultArrayName = 'element_s'
s.Function = 'material_ori_3_Y*iHat-material_ori_3_X*jHat+0*kHat'

# create a new 'Calculator'
n = Calculator(registrationName='n', Input=becas_resultscase)
n.AttributeType = 'Cell Data'
n.ResultArrayName = 'element_n'
n.Function = 'material_ori_3'

# create a new 'Glyph'
n_1 = Glyph(registrationName='n', Input=n,
    GlyphType='Arrow')
n_1.OrientationArray = ['CELLS', 'element_n']
n_1.ScaleArray = ['POINTS', 'No scale array']
n_1.ScaleFactor = 0.1
n_1.GlyphTransform = 'Transform2'
n_1.GlyphMode = 'All Points'

# create a new 'Glyph'
s_1 = Glyph(registrationName='s', Input=s,
    GlyphType='Arrow')
s_1.OrientationArray = ['CELLS', 'element_s']
s_1.ScaleArray = ['POINTS', 'No scale array']
s_1.ScaleFactor = 0.1
s_1.GlyphTransform = 'Transform2'
s_1.GlyphMode = 'All Points'

# create a new 'Glyph'
glyph4 = Glyph(registrationName='Glyph4', Input=becas_resultscase,
    GlyphType='Arrow')
glyph4.OrientationArray = ['CELLS', 'material_ori_2']
glyph4.ScaleArray = ['POINTS', 'No scale array']
glyph4.ScaleFactor = 0.1
glyph4.GlyphTransform = 'Transform2'
glyph4.GlyphMode = 'All Points'

# create a new 'Glyph'
glyph2 = Glyph(registrationName='Glyph2', Input=becas_resultscase,
    GlyphType='Arrow')
glyph2.OrientationArray = ['CELLS', 'material_ori_3']
glyph2.ScaleArray = ['POINTS', 'No scale array']
glyph2.ScaleFactor = 0.1
glyph2.GlyphTransform = 'Transform2'
glyph2.GlyphMode = 'All Points'

# create a new 'Calculator'
z = Calculator(registrationName='z', Input=becas_resultscase)
z.AttributeType = 'Cell Data'
z.ResultArrayName = 'element_z'
z.Function = '0*iHat-0*jHat+1*kHat'

# create a new 'Glyph'
z_1 = Glyph(registrationName='z', Input=z,
    GlyphType='Arrow')
z_1.OrientationArray = ['CELLS', 'element_z']
z_1.ScaleArray = ['POINTS', 'No scale array']
z_1.ScaleFactor = 0.1
z_1.GlyphTransform = 'Transform2'
z_1.GlyphMode = 'All Points'

# create a new 'Glyph'
glyph1 = Glyph(registrationName='Glyph1', Input=becas_resultscase,
    GlyphType='Arrow')
glyph1.OrientationArray = ['CELLS', 'material_ori_1']
glyph1.ScaleArray = ['POINTS', 'No scale array']
glyph1.ScaleFactor = 0.1
glyph1.GlyphTransform = 'Transform2'
glyph1.GlyphMode = 'All Points'

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from s
sDisplay = Show(s, renderView1, 'UnstructuredGridRepresentation')

# get color transfer function/color map for 'material_id'
material_idLUT = GetColorTransferFunction('material_id')
material_idLUT.RGBPoints = [1.0, 0.231373, 0.298039, 0.752941, 1.0001220703125, 0.865003, 0.865003, 0.865003, 1.000244140625, 0.705882, 0.0156863, 0.14902]
material_idLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'material_id'
material_idPWF = GetOpacityTransferFunction('material_id')
material_idPWF.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]
material_idPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
sDisplay.Representation = 'Surface'
sDisplay.ColorArrayName = ['CELLS', 'material_id']
sDisplay.LookupTable = material_idLUT
sDisplay.SelectTCoordArray = 'None'
sDisplay.SelectNormalArray = 'None'
sDisplay.SelectTangentArray = 'None'
sDisplay.OSPRayScaleArray = 'elastic_axis_1'
sDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
sDisplay.SelectOrientationVectors = 'element_s'
sDisplay.ScaleFactor = 0.1
sDisplay.SelectScaleArray = 'material_id'
sDisplay.GlyphType = 'Arrow'
sDisplay.GlyphTableIndexArray = 'material_id'
sDisplay.GaussianRadius = 0.005
sDisplay.SetScaleArray = ['POINTS', 'elastic_axis_1']
sDisplay.ScaleTransferFunction = 'PiecewiseFunction'
sDisplay.OpacityArray = ['POINTS', 'elastic_axis_1']
sDisplay.OpacityTransferFunction = 'PiecewiseFunction'
sDisplay.DataAxesGrid = 'GridAxesRepresentation'
sDisplay.PolarAxes = 'PolarAxesRepresentation'
sDisplay.ScalarOpacityFunction = material_idPWF
sDisplay.ScalarOpacityUnitDistance = 0.3598166492554378
sDisplay.OpacityArrayName = ['CELLS', 'material_id']
sDisplay.ExtractedBlockIndex = 1

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
sDisplay.ScaleTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
sDisplay.OpacityTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# show data from n
nDisplay = Show(n, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
nDisplay.Representation = 'Surface'
nDisplay.ColorArrayName = ['CELLS', 'material_id']
nDisplay.LookupTable = material_idLUT
nDisplay.SelectTCoordArray = 'None'
nDisplay.SelectNormalArray = 'None'
nDisplay.SelectTangentArray = 'None'
nDisplay.OSPRayScaleArray = 'elastic_axis_1'
nDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
nDisplay.SelectOrientationVectors = 'element_n'
nDisplay.ScaleFactor = 0.1
nDisplay.SelectScaleArray = 'material_id'
nDisplay.GlyphType = 'Arrow'
nDisplay.GlyphTableIndexArray = 'material_id'
nDisplay.GaussianRadius = 0.005
nDisplay.SetScaleArray = ['POINTS', 'elastic_axis_1']
nDisplay.ScaleTransferFunction = 'PiecewiseFunction'
nDisplay.OpacityArray = ['POINTS', 'elastic_axis_1']
nDisplay.OpacityTransferFunction = 'PiecewiseFunction'
nDisplay.DataAxesGrid = 'GridAxesRepresentation'
nDisplay.PolarAxes = 'PolarAxesRepresentation'
nDisplay.ScalarOpacityFunction = material_idPWF
nDisplay.ScalarOpacityUnitDistance = 0.3598166492554378
nDisplay.OpacityArrayName = ['CELLS', 'material_id']
nDisplay.ExtractedBlockIndex = 1

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
nDisplay.ScaleTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
nDisplay.OpacityTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# show data from z
zDisplay = Show(z, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
zDisplay.Representation = 'Surface'
zDisplay.ColorArrayName = ['CELLS', 'material_id']
zDisplay.LookupTable = material_idLUT
zDisplay.SelectTCoordArray = 'None'
zDisplay.SelectNormalArray = 'None'
zDisplay.SelectTangentArray = 'None'
zDisplay.OSPRayScaleArray = 'elastic_axis_1'
zDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
zDisplay.SelectOrientationVectors = 'element_z'
zDisplay.ScaleFactor = 0.1
zDisplay.SelectScaleArray = 'material_id'
zDisplay.GlyphType = 'Arrow'
zDisplay.GlyphTableIndexArray = 'material_id'
zDisplay.GaussianRadius = 0.005
zDisplay.SetScaleArray = ['POINTS', 'elastic_axis_1']
zDisplay.ScaleTransferFunction = 'PiecewiseFunction'
zDisplay.OpacityArray = ['POINTS', 'elastic_axis_1']
zDisplay.OpacityTransferFunction = 'PiecewiseFunction'
zDisplay.DataAxesGrid = 'GridAxesRepresentation'
zDisplay.PolarAxes = 'PolarAxesRepresentation'
zDisplay.ScalarOpacityFunction = material_idPWF
zDisplay.ScalarOpacityUnitDistance = 0.3598166492554378
zDisplay.OpacityArrayName = ['CELLS', 'material_id']
zDisplay.ExtractedBlockIndex = 1

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
zDisplay.ScaleTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
zDisplay.OpacityTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# show data from s_1
s_1Display = Show(s_1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
s_1Display.Representation = 'Surface'
s_1Display.AmbientColor = [1.0, 0.0, 0.0]
s_1Display.ColorArrayName = ['POINTS', '']
s_1Display.DiffuseColor = [1.0, 0.0, 0.0]
s_1Display.SelectTCoordArray = 'None'
s_1Display.SelectNormalArray = 'None'
s_1Display.SelectTangentArray = 'None'
s_1Display.OSPRayScaleArray = 'material_id'
s_1Display.OSPRayScaleFunction = 'PiecewiseFunction'
s_1Display.SelectOrientationVectors = 'element_s'
s_1Display.ScaleFactor = 0.10992921590805055
s_1Display.SelectScaleArray = 'material_id'
s_1Display.GlyphType = 'Arrow'
s_1Display.GlyphTableIndexArray = 'material_id'
s_1Display.GaussianRadius = 0.005496460795402527
s_1Display.SetScaleArray = ['POINTS', 'material_id']
s_1Display.ScaleTransferFunction = 'PiecewiseFunction'
s_1Display.OpacityArray = ['POINTS', 'material_id']
s_1Display.OpacityTransferFunction = 'PiecewiseFunction'
s_1Display.DataAxesGrid = 'GridAxesRepresentation'
s_1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
s_1Display.ScaleTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
s_1Display.OpacityTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# show data from n_1
n_1Display = Show(n_1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
n_1Display.Representation = 'Surface'
n_1Display.AmbientColor = [0.0, 1.0, 0.0]
n_1Display.ColorArrayName = ['POINTS', '']
n_1Display.DiffuseColor = [0.0, 1.0, 0.0]
n_1Display.SelectTCoordArray = 'None'
n_1Display.SelectNormalArray = 'None'
n_1Display.SelectTangentArray = 'None'
n_1Display.OSPRayScaleArray = 'material_id'
n_1Display.OSPRayScaleFunction = 'PiecewiseFunction'
n_1Display.SelectOrientationVectors = 'element_n'
n_1Display.ScaleFactor = 0.0998310685157776
n_1Display.SelectScaleArray = 'material_id'
n_1Display.GlyphType = 'Arrow'
n_1Display.GlyphTableIndexArray = 'material_id'
n_1Display.GaussianRadius = 0.004991553425788879
n_1Display.SetScaleArray = ['POINTS', 'material_id']
n_1Display.ScaleTransferFunction = 'PiecewiseFunction'
n_1Display.OpacityArray = ['POINTS', 'material_id']
n_1Display.OpacityTransferFunction = 'PiecewiseFunction'
n_1Display.DataAxesGrid = 'GridAxesRepresentation'
n_1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
n_1Display.ScaleTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
n_1Display.OpacityTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# show data from z_1
z_1Display = Show(z_1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
z_1Display.Representation = 'Surface'
z_1Display.AmbientColor = [0.0, 0.0, 1.0]
z_1Display.ColorArrayName = ['POINTS', '']
z_1Display.DiffuseColor = [0.0, 0.0, 1.0]
z_1Display.SelectTCoordArray = 'None'
z_1Display.SelectNormalArray = 'None'
z_1Display.SelectTangentArray = 'None'
z_1Display.OSPRayScaleArray = 'material_id'
z_1Display.OSPRayScaleFunction = 'PiecewiseFunction'
z_1Display.SelectOrientationVectors = 'element_z'
z_1Display.ScaleFactor = 0.10156135559082032
z_1Display.SelectScaleArray = 'material_id'
z_1Display.GlyphType = 'Arrow'
z_1Display.GlyphTableIndexArray = 'material_id'
z_1Display.GaussianRadius = 0.005078067779541015
z_1Display.SetScaleArray = ['POINTS', 'material_id']
z_1Display.ScaleTransferFunction = 'PiecewiseFunction'
z_1Display.OpacityArray = ['POINTS', 'material_id']
z_1Display.OpacityTransferFunction = 'PiecewiseFunction'
z_1Display.DataAxesGrid = 'GridAxesRepresentation'
z_1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
z_1Display.ScaleTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
z_1Display.OpacityTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# setup the color legend parameters for each legend in this view

# get color legend/bar for material_idLUT in view renderView1
material_idLUTColorBar = GetScalarBar(material_idLUT, renderView1)
material_idLUTColorBar.WindowLocation = 'UpperRightCorner'
material_idLUTColorBar.Title = 'material_id'
material_idLUTColorBar.ComponentTitle = ''

# set color bar visibility
material_idLUTColorBar.Visibility = 1

# show color legend
sDisplay.SetScalarBarVisibility(renderView1, True)

# show color legend
nDisplay.SetScalarBarVisibility(renderView1, True)

# show color legend
zDisplay.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# restore active source
SetActiveSource(z_1)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')