#   Copyright (c): 2024 Deutsches Zentrum fuer Luft- und Raumfahrt (DLR, German Aerospace Center) <www.dlr.de>. All rights reserved.

# state file generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1152, 1054]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [0.0, 0.0, 0.05000000074505806]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [-0.33157588320139747, -1.4324215481519902, 1.70153876957878]
renderView1.CameraFocalPoint = [-1.665796841280198e-17, -9.900289369584782e-17, 0.05000000074505803]
renderView1.CameraViewUp = [-0.3393069522861, 0.7432812009373908, 0.5765447497492179]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 0.5722976832572465
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(1152, 1054)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'EnSight Reader'
becas_resultscase = EnSightReader(registrationName='becas_results.case', CaseFileName='H:\\Daniel\\git\\PreDoCS\\tmp\\comparison\\becas\\output\\900\\load_cases\\transverse_y\\becas_results.case')
becas_resultscase.CellArrays = ['material_id', 'material_ori_1', 'material_ori_2', 'material_ori_3', 'elementnumbers', 'strain11', 'strain22', 'strain12', 'strain13', 'strain23', 'strain33', 'stress11', 'stress22', 'stress12', 'stress13', 'stress23', 'stress33', 'failure11', 'failure22', 'failure12', 'failure13', 'failure23', 'failure33']
becas_resultscase.PointArrays = ['nodenumbers', 'elastic_axis_1', 'elastic_axis_2', 'warping']

# create a new 'Glyph'
glyph2 = Glyph(registrationName='Glyph2', Input=becas_resultscase,
    GlyphType='Arrow')
glyph2.OrientationArray = ['CELLS', 'material_ori_3']
glyph2.ScaleArray = ['POINTS', 'No scale array']
glyph2.ScaleFactor = 0.1
glyph2.GlyphTransform = 'Transform2'
glyph2.GlyphMode = 'All Points'

# create a new 'Glyph'
glyph1 = Glyph(registrationName='Glyph1', Input=becas_resultscase,
    GlyphType='Arrow')
glyph1.OrientationArray = ['CELLS', 'material_ori_1']
glyph1.ScaleArray = ['POINTS', 'No scale array']
glyph1.ScaleFactor = 0.1
glyph1.GlyphTransform = 'Transform2'
glyph1.GlyphMode = 'All Points'

# create a new 'Glyph'
glyph4 = Glyph(registrationName='Glyph4', Input=becas_resultscase,
    GlyphType='Arrow')
glyph4.OrientationArray = ['CELLS', 'material_ori_2']
glyph4.ScaleArray = ['POINTS', 'No scale array']
glyph4.ScaleFactor = 0.1
glyph4.GlyphTransform = 'Transform2'
glyph4.GlyphMode = 'All Points'

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from becas_resultscase
becas_resultscaseDisplay = Show(becas_resultscase, renderView1, 'UnstructuredGridRepresentation')

# get color transfer function/color map for 'material_ori_1'
material_ori_1LUT = GetColorTransferFunction('material_ori_1')
material_ori_1LUT.RGBPoints = [0.014142099767923355, 0.231373, 0.298039, 0.752941, 0.014143053442239761, 0.865003, 0.865003, 0.865003, 0.014144007116556168, 0.705882, 0.0156863, 0.14902]
material_ori_1LUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'material_ori_1'
material_ori_1PWF = GetOpacityTransferFunction('material_ori_1')
material_ori_1PWF.Points = [0.014142099767923355, 0.0, 0.5, 0.0, 0.014144007116556168, 1.0, 0.5, 0.0]
material_ori_1PWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
becas_resultscaseDisplay.Representation = 'Surface'
becas_resultscaseDisplay.ColorArrayName = ['CELLS', 'material_ori_1']
becas_resultscaseDisplay.LookupTable = material_ori_1LUT
becas_resultscaseDisplay.SelectTCoordArray = 'None'
becas_resultscaseDisplay.SelectNormalArray = 'None'
becas_resultscaseDisplay.SelectTangentArray = 'None'
becas_resultscaseDisplay.OSPRayScaleArray = 'elastic_axis_1'
becas_resultscaseDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
becas_resultscaseDisplay.SelectOrientationVectors = 'material_ori_1'
becas_resultscaseDisplay.ScaleFactor = 0.1
becas_resultscaseDisplay.SelectScaleArray = 'material_id'
becas_resultscaseDisplay.GlyphType = 'Arrow'
becas_resultscaseDisplay.GlyphTableIndexArray = 'material_id'
becas_resultscaseDisplay.GaussianRadius = 0.005
becas_resultscaseDisplay.SetScaleArray = ['POINTS', 'elastic_axis_1']
becas_resultscaseDisplay.ScaleTransferFunction = 'PiecewiseFunction'
becas_resultscaseDisplay.OpacityArray = ['POINTS', 'elastic_axis_1']
becas_resultscaseDisplay.OpacityTransferFunction = 'PiecewiseFunction'
becas_resultscaseDisplay.DataAxesGrid = 'GridAxesRepresentation'
becas_resultscaseDisplay.PolarAxes = 'PolarAxesRepresentation'
becas_resultscaseDisplay.ScalarOpacityFunction = material_ori_1PWF
becas_resultscaseDisplay.ScalarOpacityUnitDistance = 0.3598166492554378
becas_resultscaseDisplay.OpacityArrayName = ['CELLS', 'material_id']
becas_resultscaseDisplay.ExtractedBlockIndex = 1

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
becas_resultscaseDisplay.ScaleTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
becas_resultscaseDisplay.OpacityTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# show data from glyph1
glyph1Display = Show(glyph1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
glyph1Display.Representation = 'Surface'
glyph1Display.AmbientColor = [1.0, 0.0, 0.0]
glyph1Display.ColorArrayName = ['POINTS', '']
glyph1Display.DiffuseColor = [1.0, 0.0, 0.0]
glyph1Display.SelectTCoordArray = 'None'
glyph1Display.SelectNormalArray = 'None'
glyph1Display.SelectTangentArray = 'None'
glyph1Display.OSPRayScaleArray = 'material_id'
glyph1Display.OSPRayScaleFunction = 'PiecewiseFunction'
glyph1Display.SelectOrientationVectors = 'material_ori_1'
glyph1Display.ScaleFactor = 0.10589842796325684
glyph1Display.SelectScaleArray = 'material_id'
glyph1Display.GlyphType = 'Arrow'
glyph1Display.GlyphTableIndexArray = 'material_id'
glyph1Display.GaussianRadius = 0.005294921398162842
glyph1Display.SetScaleArray = ['POINTS', 'material_id']
glyph1Display.ScaleTransferFunction = 'PiecewiseFunction'
glyph1Display.OpacityArray = ['POINTS', 'material_id']
glyph1Display.OpacityTransferFunction = 'PiecewiseFunction'
glyph1Display.DataAxesGrid = 'GridAxesRepresentation'
glyph1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
glyph1Display.ScaleTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
glyph1Display.OpacityTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# show data from glyph4
glyph4Display = Show(glyph4, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
glyph4Display.Representation = 'Surface'
glyph4Display.AmbientColor = [0.0, 1.0, 0.0]
glyph4Display.ColorArrayName = ['POINTS', '']
glyph4Display.DiffuseColor = [0.0, 1.0, 0.0]
glyph4Display.SelectTCoordArray = 'None'
glyph4Display.SelectNormalArray = 'None'
glyph4Display.SelectTangentArray = 'None'
glyph4Display.OSPRayScaleArray = 'material_id'
glyph4Display.OSPRayScaleFunction = 'PiecewiseFunction'
glyph4Display.SelectOrientationVectors = 'material_ori_1'
glyph4Display.ScaleFactor = 0.10992921590805055
glyph4Display.SelectScaleArray = 'material_id'
glyph4Display.GlyphType = 'Arrow'
glyph4Display.GlyphTableIndexArray = 'material_id'
glyph4Display.GaussianRadius = 0.005496460795402527
glyph4Display.SetScaleArray = ['POINTS', 'material_id']
glyph4Display.ScaleTransferFunction = 'PiecewiseFunction'
glyph4Display.OpacityArray = ['POINTS', 'material_id']
glyph4Display.OpacityTransferFunction = 'PiecewiseFunction'
glyph4Display.DataAxesGrid = 'GridAxesRepresentation'
glyph4Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
glyph4Display.ScaleTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
glyph4Display.OpacityTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# show data from glyph2
glyph2Display = Show(glyph2, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
glyph2Display.Representation = 'Surface'
glyph2Display.AmbientColor = [0.0, 0.0, 1.0]
glyph2Display.ColorArrayName = ['POINTS', '']
glyph2Display.DiffuseColor = [0.0, 0.0, 1.0]
glyph2Display.SelectTCoordArray = 'None'
glyph2Display.SelectNormalArray = 'None'
glyph2Display.SelectTangentArray = 'None'
glyph2Display.OSPRayScaleArray = 'material_id'
glyph2Display.OSPRayScaleFunction = 'PiecewiseFunction'
glyph2Display.SelectOrientationVectors = 'material_ori_1'
glyph2Display.ScaleFactor = 0.0998310685157776
glyph2Display.SelectScaleArray = 'material_id'
glyph2Display.GlyphType = 'Arrow'
glyph2Display.GlyphTableIndexArray = 'material_id'
glyph2Display.GaussianRadius = 0.004991553425788879
glyph2Display.SetScaleArray = ['POINTS', 'material_id']
glyph2Display.ScaleTransferFunction = 'PiecewiseFunction'
glyph2Display.OpacityArray = ['POINTS', 'material_id']
glyph2Display.OpacityTransferFunction = 'PiecewiseFunction'
glyph2Display.DataAxesGrid = 'GridAxesRepresentation'
glyph2Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
glyph2Display.ScaleTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
glyph2Display.OpacityTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# setup the color legend parameters for each legend in this view

# get color legend/bar for material_ori_1LUT in view renderView1
material_ori_1LUTColorBar = GetScalarBar(material_ori_1LUT, renderView1)
material_ori_1LUTColorBar.Title = 'material_ori_1'
material_ori_1LUTColorBar.ComponentTitle = 'Magnitude'

# set color bar visibility
material_ori_1LUTColorBar.Visibility = 1

# show color legend
becas_resultscaseDisplay.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# restore active source
SetActiveSource(glyph4)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')