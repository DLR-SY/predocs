#   Copyright (c): 2024 Deutsches Zentrum fuer Luft- und Raumfahrt (DLR, German Aerospace Center) <www.dlr.de>. All rights reserved.

from paraview.simple import *

becas_resultscase = EnSightReader(CaseFileName=r'H:\Daniel\git\PreDoCS\tmp\comparison\becas\output\900\load_cases\transverse_y\becas_results.case')
becas_resultscase.CellArrays = ['material_id', 'material_ori_1', 'material_ori_2', 'material_ori_3', 'elementnumbers', 'strain11', 'strain22', 'strain12', 'strain13', 'strain23', 'strain33', 'stress11', 'stress22', 'stress12', 'stress13', 'stress23', 'stress33', 'failure11', 'failure22', 'failure12', 'failure13', 'failure23', 'failure33']
becas_resultscase.PointArrays = ['nodenumbers', 'elastic_axis_1', 'elastic_axis_2', 'warping']

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')


# Tensor calculation
tensor_calc = [
    ('element_s', 'material_ori_3_Y*iHat-material_ori_3_X*jHat+0*kHat'),
    ('element_n', 'material_ori_3'),
    ('element_z', '0*iHat-0*jHat+1*kHat'),
    
    ('strain_ss', 'element_s_X^2*strain11 + 2*element_s_X*element_s_Y*strain12 + 2*element_s_X*element_s_Z*strain13 + element_s_Y^2*strain22 + 2*element_s_Y*element_s_Z*strain23 + element_s_Z^2*strain33'),
    ('stress_ss', 'element_s_X^2*stress11 + 2*element_s_X*element_s_Y*stress12 + 2*element_s_X*element_s_Z*stress13 + element_s_Y^2*stress22 + 2*element_s_Y*element_s_Z*stress23 + element_s_Z^2*stress33'),
    ('strain_sn', 'element_n_X*element_s_X*strain11 + element_n_Y*element_s_Y*strain22 + element_n_Z*element_s_Z*strain33 + (element_n_Y*element_s_X + element_n_X*element_s_Y)*strain12 + (element_n_Z*element_s_X + element_n_X*element_s_Z)*strain13 + (element_n_Z*element_s_Y + element_n_Y*element_s_Z)*strain23'),
    ('stress_sn', 'element_n_X*element_s_X*stress11 + element_n_Y*element_s_Y*stress22 + element_n_Z*element_s_Z*stress33 + (element_n_Y*element_s_X + element_n_X*element_s_Y)*stress12 + (element_n_Z*element_s_X + element_n_X*element_s_Z)*stress13 + (element_n_Z*element_s_Y + element_n_Y*element_s_Z)*stress23'),
    ('strain_zs', 'element_s_X*element_z_X*strain11 + element_s_Y*element_z_Y*strain22 + element_s_Z*element_z_Z*strain33 + (element_s_Y*element_z_X + element_s_X*element_z_Y)*strain12 + (element_s_Z*element_z_X + element_s_X*element_z_Z)*strain13 + (element_s_Z*element_z_Y + element_s_Y*element_z_Z)*strain23'),
    ('stress_zs', 'element_s_X*element_z_X*stress11 + element_s_Y*element_z_Y*stress22 + element_s_Z*element_z_Z*stress33 + (element_s_Y*element_z_X + element_s_X*element_z_Y)*stress12 + (element_s_Z*element_z_X + element_s_X*element_z_Z)*stress13 + (element_s_Z*element_z_Y + element_s_Y*element_z_Z)*stress23'),
    ('strain_nn', 'element_n_X^2*strain11 + 2*element_n_X*element_n_Y*strain12 + 2*element_n_X*element_n_Z*strain13 + element_n_Y^2*strain22 + 2*element_n_Y*element_n_Z*strain23 + element_n_Z^2*strain33'),
    ('stress_nn', 'element_n_X^2*stress11 + 2*element_n_X*element_n_Y*stress12 + 2*element_n_X*element_n_Z*stress13 + element_n_Y^2*stress22 + 2*element_n_Y*element_n_Z*stress23 + element_n_Z^2*stress33'),
    ('strain_zn', 'element_n_X*element_z_X*strain11 + element_n_Y*element_z_Y*strain22 + element_n_Z*element_z_Z*strain33 + (element_n_Y*element_z_X + element_n_X*element_z_Y)*strain12 + (element_n_Z*element_z_X + element_n_X*element_z_Z)*strain13 + (element_n_Z*element_z_Y + element_n_Y*element_z_Z)*strain23'),
    ('stress_zn', 'element_n_X*element_z_X*stress11 + element_n_Y*element_z_Y*stress22 + element_n_Z*element_z_Z*stress33 + (element_n_Y*element_z_X + element_n_X*element_z_Y)*stress12 + (element_n_Z*element_z_X + element_n_X*element_z_Z)*stress13 + (element_n_Z*element_z_Y + element_n_Y*element_z_Z)*stress23'),
    ('strain_zz', 'element_z_X^2*strain11 + 2*element_z_X*element_z_Y*strain12 + 2*element_z_X*element_z_Z*strain13 + element_z_Y^2*strain22 + 2*element_z_Y*element_z_Z*strain23 + element_z_Z^2*strain33'),
    ('stress_zz', 'element_z_X^2*stress11 + 2*element_z_X*element_z_Y*stress12 + 2*element_z_X*element_z_Z*stress13 + element_z_Y^2*stress22 + 2*element_z_Y*element_z_Z*stress23 + element_z_Z^2*stress33') ]

input_filter = becas_resultscase
for result_name, function_string in tensor_calc:
    calculator = Calculator(Input=input_filter)
    calculator.AttributeType = 'Cell Data'
    calculator.ResultArrayName = result_name
    calculator.Function = function_string
    input_filter = calculator
	
# Create calculator for arrow vector
arrow_vector = Calculator(Input=input_filter)
arrow_vector.AttributeType = 'Cell Data'
arrow_vector.ResultArrayName = 'vec'
arrow_vector.Function = 'element_s*stress_zs'

# Create arrow glyph
arrow_glyph = Glyph(Input=arrow_vector, GlyphType='Cone')
#arrow_glyph.Scalars = None
arrow_glyph.OrientationArray = ['CELLS', 'vec']
arrow_glyph.ScaleFactor = 0.01
arrow_glyph.GlyphMode = 'Every Nth Point'
arrow_glyph.Stride = 1
