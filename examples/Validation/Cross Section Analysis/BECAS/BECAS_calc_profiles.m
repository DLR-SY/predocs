function [ ] = BECAS_calc_profiles(profiles, calc_load_cases, input_path, output_path)

%% Setup path for BECAS library

%Set base path
[ BECAS_basepath ] = BECAS_initialize( );

%Set folder path
addpath(genpath(fullfile(BECAS_basepath,'src','matlab')))
addpath(genpath(fullfile(BECAS_basepath,'..','FRANS','src','matlab')))
addpath(genpath(fullfile(BECAS_basepath,'..','SHEFE','src','matlab')))

%% Displacement reactions
load_cases.transverse_x = [1 0 0 0 0 0];
load_cases.transverse_y = [0 1 0 0 0 0];
load_cases.extension = [0 0 1 0 0 0];
load_cases.bending_x = [0 0 0 1 0 0];
load_cases.bending_y = [0 0 0 0 1 0];
load_cases.torsion = [0 0 0 0 0 1];

for p = 1:length(profiles)
    profile_id = profiles(p);
    profile_name = int2str(profile_id);
    profile_name
    input_folder = strcat(input_path, profile_name, '/BECAS_SECTION');
    options.foldername = input_folder;
    tic
    [ utils ] = BECAS_Utils( options );
    %BECAS_PlotInput( 0, utils )
    [constitutive.Ks,solutions] = BECAS_Constitutive_Ks(utils);
    [constitutive.Ms]=BECAS_Constitutive_Ms(utils);
    [ csprops ] = BECAS_CrossSectionProps(constitutive.Ks,utils);
    persist_data.calculation_time.cross_section_calculation = toc;
    persist_data.elastic_center = [csprops.ElasticX, csprops.ElasticY];
    persist_data.shear_center = [csprops.ShearX, csprops.ShearY];
    persist_data.stiffness_matrix = constitutive.Ks;
    persist_data.mass_matrix = constitutive.Ms;
    persist_data.principal_axis_angle = csprops.AlphaPrincipleAxis_ElasticCenter;
    
    if calc_load_cases
        %% Recover strains and stress for a given load case
        load_case_names = fieldnames(load_cases);
        for lc = 1:length(load_case_names)
            %Load vector
            load_case_name = load_case_names{lc};
            theta0 = load_cases.(load_case_name);
            
            tic
            %Calculate strains
            [ strain ] = BECAS_RecoverStrains( theta0, solutions, utils );

            %Calculate stresses
            [ stress ] = BECAS_RecoverStresses( strain, utils );
            persist_data.calculation_time.load_cases_calculation.(load_case_name) = toc;
            %Check failure criteria
            [ failureElement ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );

            warping=solutions.X*theta0';
            dir = strcat(output_path, profile_name, '/load_cases/', load_case_name, '/');
            BECAS_PARAVIEW( dir, utils, csprops, warping, strain.GlobalElement, stress.GlobalElement, failureElement )
        end
    end
    
    json = jsonencode(persist_data);
    fileID = fopen(strcat(output_path, profile_name, '.json'), 'w');
    fprintf(fileID,json);
    fclose(fileID);
end

end

