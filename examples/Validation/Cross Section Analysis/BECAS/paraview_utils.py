#   Copyright (c): 2024 Deutsches Zentrum fuer Luft- und Raumfahrt (DLR, German Aerospace Center) <www.dlr.de>. All rights reserved.

from paraview.simple import *


def apply_tensor_calculation_filter(input_filter, max_bar_length):

    # Tensor calculation
    tensor_calc = [
        ('element_s', 'norm(material_ori_3_Y*iHat-material_ori_3_X*jHat+0*kHat)'),
        ('element_n', 'norm(material_ori_3)'),
        ('element_z', 'norm(0*iHat-0*jHat+1*kHat)'),

        ('bar_dir', 'element_n' if max_bar_length > 0 else '-element_n'),

        ('strain_ss',
         'element_s_X^2*strain11 + 2*element_s_X*element_s_Y*strain12 + 2*element_s_X*element_s_Z*strain13 + element_s_Y^2*strain22 + 2*element_s_Y*element_s_Z*strain23 + element_s_Z^2*strain33'),
        ('stress_ss',
         'element_s_X^2*stress11 + 2*element_s_X*element_s_Y*stress12 + 2*element_s_X*element_s_Z*stress13 + element_s_Y^2*stress22 + 2*element_s_Y*element_s_Z*stress23 + element_s_Z^2*stress33'),
        ('strain_sn',
         'element_n_X*element_s_X*strain11 + element_n_Y*element_s_Y*strain22 + element_n_Z*element_s_Z*strain33 + (element_n_Y*element_s_X + element_n_X*element_s_Y)*strain12 + (element_n_Z*element_s_X + element_n_X*element_s_Z)*strain13 + (element_n_Z*element_s_Y + element_n_Y*element_s_Z)*strain23'),
        ('stress_sn',
         'element_n_X*element_s_X*stress11 + element_n_Y*element_s_Y*stress22 + element_n_Z*element_s_Z*stress33 + (element_n_Y*element_s_X + element_n_X*element_s_Y)*stress12 + (element_n_Z*element_s_X + element_n_X*element_s_Z)*stress13 + (element_n_Z*element_s_Y + element_n_Y*element_s_Z)*stress23'),
        ('strain_zs',
         'element_s_X*element_z_X*strain11 + element_s_Y*element_z_Y*strain22 + element_s_Z*element_z_Z*strain33 + (element_s_Y*element_z_X + element_s_X*element_z_Y)*strain12 + (element_s_Z*element_z_X + element_s_X*element_z_Z)*strain13 + (element_s_Z*element_z_Y + element_s_Y*element_z_Z)*strain23'),
        ('stress_zs',
         'element_s_X*element_z_X*stress11 + element_s_Y*element_z_Y*stress22 + element_s_Z*element_z_Z*stress33 + (element_s_Y*element_z_X + element_s_X*element_z_Y)*stress12 + (element_s_Z*element_z_X + element_s_X*element_z_Z)*stress13 + (element_s_Z*element_z_Y + element_s_Y*element_z_Z)*stress23'),
        ('strain_nn',
         'element_n_X^2*strain11 + 2*element_n_X*element_n_Y*strain12 + 2*element_n_X*element_n_Z*strain13 + element_n_Y^2*strain22 + 2*element_n_Y*element_n_Z*strain23 + element_n_Z^2*strain33'),
        ('stress_nn',
         'element_n_X^2*stress11 + 2*element_n_X*element_n_Y*stress12 + 2*element_n_X*element_n_Z*stress13 + element_n_Y^2*stress22 + 2*element_n_Y*element_n_Z*stress23 + element_n_Z^2*stress33'),
        ('strain_zn',
         'element_n_X*element_z_X*strain11 + element_n_Y*element_z_Y*strain22 + element_n_Z*element_z_Z*strain33 + (element_n_Y*element_z_X + element_n_X*element_z_Y)*strain12 + (element_n_Z*element_z_X + element_n_X*element_z_Z)*strain13 + (element_n_Z*element_z_Y + element_n_Y*element_z_Z)*strain23'),
        ('stress_zn',
         'element_n_X*element_z_X*stress11 + element_n_Y*element_z_Y*stress22 + element_n_Z*element_z_Z*stress33 + (element_n_Y*element_z_X + element_n_X*element_z_Y)*stress12 + (element_n_Z*element_z_X + element_n_X*element_z_Z)*stress13 + (element_n_Z*element_z_Y + element_n_Y*element_z_Z)*stress23'),
        ('strain_zz',
         'element_z_X^2*strain11 + 2*element_z_X*element_z_Y*strain12 + 2*element_z_X*element_z_Z*strain13 + element_z_Y^2*strain22 + 2*element_z_Y*element_z_Z*strain23 + element_z_Z^2*strain33'),
        ('stress_zz',
         'element_z_X^2*stress11 + 2*element_z_X*element_z_Y*stress12 + 2*element_z_X*element_z_Z*stress13 + element_z_Y^2*stress22 + 2*element_z_Y*element_z_Z*stress23 + element_z_Z^2*stress33')]

    for result_name, function_string in tensor_calc:
        calculator = Calculator(Input=input_filter)
        calculator.AttributeType = 'Cell Data'
        calculator.ResultArrayName = result_name
        calculator.Function = function_string
        input_filter = calculator

    return input_filter