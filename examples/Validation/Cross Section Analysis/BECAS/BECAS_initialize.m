function [ BECAS_basepath ] = BECAS_initialize( BECAS_basepath_user )
%********************************************************
% File: BECAS_initialize
%   This function tries to return the name of the BECAS installation
%   directory. The BECAS installation directory is the directory containing 
%   the file BECAS_installation_directory.txt
%   If a path is given as function argument, this path is returned.
%   If not, the content of the environment variable BECAS_PATH is returned.
%   If the environment variable BECAS_PATH is not found, the standard
%   directory C:\BECAS is returned.
%   If C:\BECAS does not exist, an error message is generated.
%
% Syntax:
%   [ BECAS_basepath ] = BECAS_initialize( BECAS_basepath_user )
%
% Date:
%   Version 1.0    27.10.2014   Robert Bitsche
%
% (c) DTU Wind Energy
%********************************************************

%Define standard path and check
if ispc
    standardpath = 'C:\\BECAS';
elseif isunix
    path_home = getenv('HOME');
    standardpath = fullfile(path_home,'/BECAS');
end

%Get path from environment variable if path exists
path_from_env_variable = getenv('BECAS_PATH');

%
if nargin > 0 % path was given as function argument
    BECAS_basepath = BECAS_basepath_user;
elseif path_from_env_variable  % environment varialbe was defined
    BECAS_basepath = path_from_env_variable;
elseif exist(standardpath,'dir')==7 % standard directory exists
    BECAS_basepath = standardpath;
else
    errormessage = sprintf( ...
                   ['The BECAS installation directory could not be determined.\n' ...
                    'The BECAS installation directory is the directory containing the file \n' ...
                    'BECAS_installation_directory.txt\n' ...
                    'Please do ONE of the following:\n' ...
                    '  * Call the function %s with the BECAS installation directory as argument.\n' ...
                    '  * Create an environment variable called BECAS_PATH containing the BECAS installation directory.\n' ...
                    '    (Matlab restart required.)\n' ...
                    '  * Move BECAS so that the BECAS installation directory is %s\n'], mfilename, standardpath );
    error('BECAS:BasepathUndefined',errormessage');    
end

% Check if found directory exists
if exist(BECAS_basepath,'dir')~=7
    error('%s is not a directory.',BECAS_basepath);
end

end



